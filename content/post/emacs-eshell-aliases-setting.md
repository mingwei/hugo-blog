+++
title = "Emacs Eshell Aliases Setting"
author = "Mingwei Zhang"
date = 2018-10-04T09:27:00-07:00
categories = ["tech"]
draft = false
+++

Setting Aliases in Eshell and be done programmatically in an elisp
configuration. To do so, you will need to invoke the `eshell/alias` function
from your elisp script file.

Here is an example of a simple aliases configuration:

```emacs-lisp
;; open files
(eshell/alias "ff" "find-file $1")
(eshell/alias "fw" "find-file-other-window $1")
(eshell/alias "fr" "find-file-other-frame $1")

;; list files
(eshell/alias "ll" "ls -la $*")
(eshell/alias "la" "ls -a $*")
```

You can copy/migrate your `.bash_alises` configs here accordingly.

Reference:

-   <https://caiorss.github.io/Emacs-Elisp-Programming/Eshell.html#sec-1-13>
