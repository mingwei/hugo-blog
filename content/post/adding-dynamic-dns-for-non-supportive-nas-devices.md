+++
title = "Adding Dynamic DNS for Non-supportive NAS Devices"
author = ["Mingwei Zhang"]
date = 2017-05-23T00:00:00-07:00
tags = ["nas", "dns"]
categories = ["tech"]
draft = false
+++

Network Attached System (NAS) is a good invention that makes managing and sharing our daily lives so much more convenient.
In general, I like the idea of having a storage device connected to the Internet without relying on cloud storage or a always-on heavy PC.

However, people would need a domain name to bind to the IP address of the NAS in order to remember it.
If you bought a domain name from some providers (like Google Domains or GoDaddy), and bound a subdomain to the IP address of your NAS, is the problem solved?

Unfortunately, the answer is no for many people.


## Problem {#problem}

The problem lies not in the binding of the domain name to IP address, but rather the nature that ****IP addresses change all the time**** for many users.
For example, if you are researcher like me in an University, you can utilize the University's network to get a public facing IP addresses.
But you will also most likely not get a permanent assignment of the IP address,
meaning that whenever you reboot your device or experiencing power outages,
it is likely that your device will obtain a different IP address when it came back online.
With new address and old binding, your DNS solution will not work anymore.

For machines with more control, we can use dynamic DNS software like `ddclient` to
dynamically update your DNS provider's IP binding whenever there is change detected.
But with boxes like WD NAS boxes, where it is very hard, if not impossible,
to install any new software onto them, we are not able to run `ddclient` anymore.


## Step 0: Make sure your NAS box's SSH feature is enabled {#step-0-make-sure-your-nas-box-s-ssh-feature-is-enabled}

This solution requires you login to your NAS box using SSH.
For my WD NAS, it has a SSH option default disabled.
Make sure you enable that, and test logging into it before proceeding.

If you are not familiar with SSH, go check out this tutorial:
<https://www.digitalocean.com/community/tutorials/how-to-use-ssh-to-connect-to-a-remote-server-in-ubuntu>


## Step 1: Register a domain in DuckDNS {#step-1-register-a-domain-in-duckdns}

DuckDNS is a fantastic free tool that works as a free DNS server for your own devices.
It also provides a \`cron\`-based workflow that can help periodically update device's IP address onto its service,
using only the basic `crontab` and `curl` command, which I believe all basic Linux boxes should have already installed.

{{< figure src="https://i.imgur.com/jZjNb8m.png" >}}


## Step 2: Install service on your NAS box {#step-2-install-service-on-your-nas-box}

After register a new domain, remember to click `install` on to menu bar, and follow the instructions in `cron linux` section.
All the operations needed here need to be performed on your NAS box over SSH.

{{< figure src="https://i.imgur.com/q8dPbYt.png" >}}

If you successfully finished all the steps here, you should be able to see your NAS's IP address on the DuckDNS control panel.


## Step 3: [optional] Forward domain name {#step-3-optional-forward-domain-name}

Note that, the domain name you get looks like `something.duckdns.org`.
If you wish to use your own domain name,
then you need to configure domain name forwarding on your DNS service provider.

Let me know if you have any comments. Enjoy your day!
