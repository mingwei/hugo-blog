+++
title = "Install ox-hugo directly from GitHub source code"
author = ["Mingwei Zhang"]
date = 2018-04-04T10:17:00-07:00
tags = ["blog", "emacs", "org", "mode"]
categories = ["tech"]
draft = false
+++

`ox-hugo` ([github link](https://github.com/kaushalmodi/ox-hugo)) is a fantastic package that can help transform `org-mode` notes into HUGO blog posts.
The installation is very easy if you use vanilla Emacs (i.e. just use MELPA).

However, on the current develop branch of Spacemacs,
I encountered the problem of dependency mismatch where `ox-hugo` requires `org` package
while `Spacemacs` uses `org-plus-contrib` package.
As a result, on starting Spacemacs it will try to delete `org` package and the reinstall `org` after it tries to load `ox-hugo`.

After some digging, I believe I have a workaround.

Steps:

1.  checkout `ox-hugo` to local directory
2.  add `use-packge` command to load the package

The following is my current configuration added in Spacemacs to load `ox-hugo`.

```lisp
(use-package ox-hugo
  :load-path "~/LOCAL_PATH"
  :after (org)
  :config
  (setq org-hugo-default-section-directory "posts")
  (defun org-hugo-new-subtree-post-capture-template ()
    "Returns `org-capture' template string for new Hugo post.
     See `org-capture-templates' for more information."
    (let* ((title (read-from-minibuffer "Post Title: ")) ;Prompt to enter the post title
           (fname (org-hugo-slug title)))
      (mapconcat #'identity
                 `(
                   ,(concat "* TODO " title)
                   ":PROPERTIES:"
                   ,(concat ":EXPORT_FILE_NAME: " fname)
                   ":END:"
                   "%?\n")          ;Place the cursor here finally
                 "\n")))

  (add-to-list 'org-capture-templates
               '("h"                ;`org-capture' binding + h
                 "Hugo post"
                 entry
                 ;; It is assumed that below file is present in `org-directory'
                 ;; and that it has a "Blog Ideas" heading. It can even be a
                 ;; symlink pointing to the actual location of all-posts.org!
                 (file+olp "personal/blog.org" "Blog Ideas")
                 (function org-hugo-new-subtree-post-capture-template)))
  )
```

`:load-path` sets the local path it should load from.
`:after (org)` allows the `ox-hugo` only be loaded after `org` is loaded.
