+++
title = "Filtering by categories for org-mode agenda view"
author = ["Mingwei Zhang"]
date = 2018-09-26T11:40:00-07:00
tags = ["emacs", "org"]
categories = ["tech"]
draft = false
+++

Customizing agenda view helps me to better visualize the todo items for work and
better schedule things and maintain sanity.

It is very convient to set a category property to the top-level section of a org
file, since the category carries onto all its subtree entries recursively. As a
result, all todo items in the agenda view will have category as prefix and
therefore very clear on what project the todo item belongs to.

To further clear things up, I'd then want to put all things with the same
category into one block. It is not very clear from all the documentations I read
so far on how to achieve this, therefore this post.

To do so, in your customization code, add the following or similar snippet.

```emacs-lisp
(tags-todo "CATEGORY=\"my-category\""
           (
            (org-agenda-overriding-header "my-category:")
            (org-agenda-prefix-format "  %-12c: ")
            (org-agenda-todo-keyword-format "%-4s")
            )
           )
```
