+++
title = "A DDoS Ransom Incident"
author = ["Mingwei Zhang"]
date = 2016-02-20T01:42:00-08:00
tags = ["ddos", "analysis"]
categories = ["research"]
draft = false
+++

Recently, I've come across [a set of discussion](<http://mailman.nanog.org/pipermail/nanog/2015-December/082739.html>) happened on the NANOG mailing list about a DDoS ransom incident. I find it rather interesting, and summarized a few notes about this incident.


## What happened? {#what-happened}

-   A small company has received a ransom note from a very well-known group for potential DDoS attacks.
-   The magnitude could be several hundred Gbps.
-   Asked on NANOG mailing list for help.
-   The attack group could most likely to be **Armada Collective**
    -   Primarily DNS and NTP amplicafication
    -   Web requests of 80 million per hour

<!-- more -->


## Suggestions from NANOG members {#suggestions-from-nanog-members}

-   Roland Dobbins from Arbor:
    -   **Upstream ACL** (access control list)
-   Nick:
    -   If they pay the ransom:
        -   the attackers can come back at any time for more.
        -   making the ransom a bigger business and aflicting more companies.
-   Patrick Darden from p66:
    -   Contact FBI or other law enforcement agencies
    -   Contact EFF (Electronic Frontier Foundation)
-   John Kristoff from Cymru:
    -   free community service UTRS (Unwanted Traffic Removal Service), or RTBH (remotely-triggered black hole filtering)
        -   <http://www.team-cymru.org/UTRS/>
        -   <https://www.iab.org/wp-content/IAB-uploads/2015/04/CARIS_2015_submission_20.pdf>
        -   utilize BGP to distributes verified BGP-based filter rules
        -   provide sample traffic (pcap, flow, logs)
-   Bill Herrin from Dirtside Systems:
    -   Whoever announces the prefix through BGP should be responsible for the defense
    -   Cloudflare can filter web-based attacks, incapsula can take care of IP-based filtering


## Summary {#summary}

-   Most admins agree that there is nothing much this company can do but to ask for help from its **upstream Internet provider**
-   Defense attacks with such maginitude (hundreds of Gbps) at the victim side is not effective
-   There are BGP-based methods, such as **RTBH** (remotely-triggered black hole filtering), that can work on prefix-level filtering or redirection.
    -   But this method can incur high collateral damage depends on the size of the prefixes.
    -   It also requires high level of trust of this kind of collaboration (filtering traffic to downstream is not usually ecnomically benefitial to the ASes)
