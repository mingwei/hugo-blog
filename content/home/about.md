+++
# About/Biography widget.
widget = "about"
active = true
date = "2016-04-20T00:00:00"

# Order that this section will appear in.
weight = 5

# List your academic interests.
[interests]
  interests = [
    "Routing Security",
    "Internet Privacy",
    "Traffic Measurement",
    "Software-Defined Networking",
  ]

# List your qualifications (such as academic degrees).
[[education.courses]]
  course = "Ph.D. Candidate in Computer Science (expected)"
  institution = "University of Oregon"
  year = 2018

[[education.courses]]
  course = "BSc in Network Engineering"
  institution = "Beijing University of Posts and Telecommunications"
  year = 2012

+++

# Biography

Mingwei Zhang is an Internet data scientist at Center for Applied Internet Data
Analysis (CAIDA), UCSD. He is also currently a Ph.D. candidate at the University of Oregon pursuing a PhD degree in Computer Science.
He is working with Professor Jun Li on various network related topics such as Internet routing,
anomaly detection systems, software-defined networking and other networking technologies.
