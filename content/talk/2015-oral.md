+++
title = "On the state of intra-domain and intra-domain routing security"
date = 2015-12-04T10:00:00-06:00
draft = false

# Talk start and end times.
#   End time can optionally be hidden by prefixing the line with `#`.
time_start = 2015-12-04T10:00:00-06:00
time_end = 2015-12-04T12:00:00-06:00

# Abstract and optional shortened version.
abstract = "Routing is a key component for building an interconnected network architecture. There are inter-domain and intra-domain routing protocols. The inter-domain routing protocol has experienced increasingly frequent anomalies, such as IP prefix hijackings, route leaks, or impact from large-scale disruptive routing events. The intra-domain routing also suffers from various attacks originated from within an autonomous system, such as topology manipulation and host-based flooding attack. Security upgrades to the existing protocols and accurate detection mechanisms have therefore been proposed and experienced. In this study, we conduct a comprehensive survey on the existing security mechanisms for both inter-domain and intra-domain routing protocols. For inter-domain routing protocol, we study the de facto protocol – Border Gateway Protocol (BGP). For intra-domain routing protocol, we investigate the recent software-domain networking paradigm and the OpenFlow protocol. For each routing protocol, we investigate both attack prevention solutions and attack detection solutions. We summarize the strengths and weaknesses of every existing solution, and discuss the missing gaps that need further research."
abstract_short = ""

# Name of event and optional event URL.
event = "Ph.D. Area Exam"
event_url = "https://cs.uoregon.edu/area-exam/state-inter-domain-and-intra-domain-routing-security"

# Location of event.
location = "Computer and Information Science, University of Oregon"

# Is this a selected talk? (true/false)
selected = false

# Projects (optional).
#   Associate this talk with one or more of your projects.
#   Simply enter the filename (excluding '.md') of your project file in `content/project/`.
#   E.g. `projects = ["deep-learning"]` references `content/project/deep-learning.md`.
projects = []

# Tags (optional).
#   Set `tags = []` for no tags, or use the form `tags = ["A Tag", "Another Tag"]` for one or more tags.
tags = []

# Links (optional).
url_pdf = ""
url_slides = "talk/2015-oral.pdf"
url_video = ""
url_code = ""

# Does the content use math formatting?
math = false

# Does the content use source code highlighting?
highlight = true

# Featured image
# Place your image in the `static/img/` folder and reference its filename below, e.g. `image = "example.jpg"`.
[header]
image = ""
caption = ""

+++
