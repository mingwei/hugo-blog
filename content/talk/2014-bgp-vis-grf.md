+++
title = "Internet Routing Anomaly Detection and Visualization"
date = 2014-03-07
draft = false

# Talk start and end times.
#   End time can optionally be hidden by prefixing the line with `#`.
# time_start = 2014-03-01T18:06:13-06:00
# time_end = 2017-12-31T18:06:13-06:00

# Abstract and optional shortened version.
abstract = "An IP prefix (i.e., a block of IP addresses) can be subject to many types of routing anomalies. One of the most infamous of such anomalies is prefix hijacking, in which an attacker hijacks traffic meant to reach the legitimate user of a prefix. Route leaks is another common type of prefix anomaly, in which a misconfigured ISP advertises illegitimate routes for prefixes. These anomalies threaten every prefix on the Internet, whether commercial or private. Unfortunately, it is unlikely that these prefix anomalies will be resolved in the near future."
abstract_short = ""

# Name of event and optional event URL.
event = "Poster Session, 2014 Graduate Research Forum, University of Oregon"
event_url = "http://blogs.uoregon.edu/gradforum/files/2014/03/2014-Program-Digital-Format-1lg8ofk.pdf"

# Location of event.
location = ""

# Is this a selected talk? (true/false)
selected = false

# Projects (optional).
#   Associate this talk with one or more of your projects.
#   Simply enter the filename (excluding '.md') of your project file in `content/project/`.
#   E.g. `projects = ["deep-learning"]` references `content/project/deep-learning.md`.
projects = []

# Tags (optional).
#   Set `tags = []` for no tags, or use the form `tags = ["A Tag", "Another Tag"]` for one or more tags.
tags = []

# Links (optional).
url_pdf = "talk/2014-bgp-vis-grf.pdf"
url_slides = ""
url_video = ""
url_code = ""

# Does the content use math formatting?
math = false

# Does the content use source code highlighting?
highlight = true

# Featured image
# Place your image in the `static/img/` folder and reference its filename below, e.g. `image = "example.jpg"`.
[header]
#image = "talk/2014-bgp-vis-grf.png"
caption = ""

+++
