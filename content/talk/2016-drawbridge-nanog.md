+++
title = "DrawBridge Leveraging Software-Defined Networking for DDoS Defense"
date = 2016-02-09
draft = false

# Talk start and end times.
#   End time can optionally be hidden by prefixing the line with `#`.
time_start = 2016-02-09T17:00:00-06:00
time_end = 2016-02-09T17:30:00-06:00

# Abstract and optional shortened version.
abstract = ""
abstract_short = ""

# Name of event and optional event URL.
event = "Lightning Talks, NANOG 66"
event_url = "https://nanog.org/meetings/nanog66/agenda"

# Location of event.
location = "San Diego, California"

# Is this a selected talk? (true/false)
selected = false

# Projects (optional).
#   Associate this talk with one or more of your projects.
#   Simply enter the filename (excluding '.md') of your project file in `content/project/`.
#   E.g. `projects = ["deep-learning"]` references `content/project/deep-learning.md`.
projects = []

# Tags (optional).
#   Set `tags = []` for no tags, or use the form `tags = ["A Tag", "Another Tag"]` for one or more tags.
tags = []

# Links (optional).
url_pdf = ""
url_slides = "talk/2016-drawbridge-nanog.pdf"
url_video = "https://youtu.be/ZG_87XLHk2Y?t=17m28s"
url_code = ""

# Does the content use math formatting?
math = false

# Does the content use source code highlighting?
highlight = true

# Featured image
# Place your image in the `static/img/` folder and reference its filename below, e.g. `image = "example.jpg"`.
[header]
image = ""
caption = ""

+++
