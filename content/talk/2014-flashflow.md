+++
title = "FlashFlow: a GPU-based Fully Programmable OpenFlow Switch"
date = 2014-05-01
draft = false

# Talk start and end times.
#   End time can optionally be hidden by prefixing the line with `#`.
# time_start = 2017-12-31T18:07:21-06:00
# time_end = 2017-12-31T18:07:21-06:00

# Abstract and optional shortened version.
abstract = ""
abstract_short = ""

event = "2014 Graduate Research Poster Contest"
event_url = ""

# Location of event.
location = "Computer and Information Science, University of Oregon"

# Is this a selected talk? (true/false)
selected = false

# Projects (optional).
#   Associate this talk with one or more of your projects.
#   Simply enter the filename (excluding '.md') of your project file in `content/project/`.
#   E.g. `projects = ["deep-learning"]` references `content/project/deep-learning.md`.
projects = []

# Tags (optional).
#   Set `tags = []` for no tags, or use the form `tags = ["A Tag", "Another Tag"]` for one or more tags.
tags = []

# Links (optional).
url_pdf = "talk/2014-flashflow-cis.pdf"
url_slides = ""
url_video = ""
url_code = ""

# Does the content use math formatting?
math = false

# Does the content use source code highlighting?
highlight = true

# Featured image
# Place your image in the `static/img/` folder and reference its filename below, e.g. `image = "example.jpg"`.
[header]
image = ""
caption = ""

+++
