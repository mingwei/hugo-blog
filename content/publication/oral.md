+++
title = "On the state of intra-domain and intra-domain routing security"
date = 2015-12-01
draft = false

# Authors. Comma separated list, e.g. `["Bob Smith", "David Jones"]`.
authors = ["Mingwei Zhang"]

# Publication type.
# Legend:
# 0 = Uncategorized
# 1 = Conference proceedings
# 2 = Journal
# 3 = Work in progress
# 4 = Technical report
# 5 = Book
# 6 = Book chapter
publication_types = ["4"]

# Publication name and optional abbreviated version.
publication = "In Computer and Information Science, University of Oregon"
publication_short = "In CIS, UO"

# Abstract and optional shortened version.
abstract = "Routing is a key component for building an interconnected network architecture. There are inter-domain and intra-domain routing protocols. The inter-domain routing protocol has experienced increasingly frequent anomalies, such as IP prefix hijackings, route leaks, or impact from large-scale disruptive routing events. The intra-domain routing also suffers from various attacks originated from within an autonomous system, such as topology manipulation and host-based flooding attack. Security upgrades to the existing protocols and accurate detection mechanisms have therefore been proposed and experienced. In this study, we conduct a comprehensive survey on the existing security mechanisms for both inter-domain and intra-domain routing protocols. For inter-domain routing protocol, we study the de facto protocol – Border Gateway Protocol (BGP). For intra-domain routing protocol, we investigate the recent software-domain networking paradigm and the OpenFlow protocol. For each routing protocol, we investigate both attack prevention solutions and attack detection solutions. We summarize the strengths and weaknesses of every existing solution, and discuss the missing gaps that need further research."
abstract_short = ""

# Featured image thumbnail (optional)
image_preview = ""

# Is this a selected publication? (true/false)
selected = false

# Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter the filename (excluding '.md') of your project file in `content/project/`.
#   E.g. `projects = ["deep-learning"]` references `content/project/deep-learning.md`.
projects = []

# Tags (optional).
#   Set `tags = []` for no tags, or use the form `tags = ["A Tag", "Another Tag"]` for one or more tags.
tags = []

# Links (optional).
url_pdf = "https://www.cs.uoregon.edu/Reports/AREA-201512-Zhang.pdf"
url_preprint = ""
url_code = ""
url_dataset = ""
url_project = ""
url_slides = ""
url_video = ""
url_poster = ""
url_source = ""

# Custom links (optional).
#   Uncomment line below to enable. For multiple links, use the form `[{...}, {...}, {...}]`.
# url_custom = [{name = "Custom Link", url = "http://example.org"}]

# Does this page contain LaTeX math? (true/false)
math = false

# Does this page require source code highlighting? (true/false)
highlight = true

# Featured image
# Place your image in the `static/img/` folder and reference its filename below, e.g. `image = "example.jpg"`.
[header]
image = ""
caption = ""

+++
