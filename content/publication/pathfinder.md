+++
title = "PathFinder: Capturing DDoS Traffic Footprints on the Internet"
date = "2018-03-07"

# Authors. Comma separated list, e.g. `["Bob Smith", "David Jones"]`.
authors = ["Lumin Shi", "Mingwei Zhang", "Jun Li", "Peter Reiher"]

# Publication type.
# Legend:
# 0 = Uncategorized
# 1 = Conference proceedings
# 2 = Journal
# 3 = Work in progress
# 4 = Technical report
# 5 = Book
# 6 = Book chapter
publication_types = ["1"]

# Publication name and optional abbreviated version.
publication = "Accepted in *International Federation for Information Processing (IFIP) Networking 2018*."
publication_short = "In *IFIP 2018*"

# Abstract and optional shortened version.
abstract = "While distributed denial-of-service (DDoS) attacks are easy to launch and are becoming more damaging, the defense against DDoS attacks often suffers from the lack of relevant knowledge of the DDoS traffic, including the paths the DDoS traffic has used, the source addresses (spoofed or not) that appear along each path, and the amount of traffic per path or per source. Though IP traceback and path inference approaches could be considered, they are either expensive and hard to deploy or inaccurate. We propose PathFinder, a service that a DDoS defense system can use to obtain the footprints of the DDoS traffic to the victim as is. It introduces a PFTrie data structure with multiple design features to log traffic at line rate, and is easy to implement and deploy on today's Internet. We show that PathFinder can significantly improve the efficacy of a DDoS defense system, while PathFinder itself is fast and has a manageable overhead, as shown in the evaluations via both synthetic and real-world DDoS traces."
abstract_short = ""

# Featured image thumbnail (optional)
image_preview = ""

# Is this a selected publication? (true/false)
selected = true

# Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter the filename (excluding '.md') of your project file in `content/project/`.
# projects = ["iseismograph"]

# Links (optional).
# url_pdf = "http://ix.cs.uoregon.edu/~mingwei/zhang17iseismograph.pdf"
# url_pdf = "http://eprints.soton.ac.uk/352095/1/Cushen-IMV2013.pdf"
# url_preprint = "http://eprints.soton.ac.uk/352095/1/Cushen-IMV2013.pdf"
# url_code = "#"
# url_dataset = "#"
# url_project = "#"
# url_slides = "#"
# url_video = "#"
# url_poster = "#"
# url_source = "#"

# Custom links (optional).
#   Uncomment line below to enable. For multiple links, use the form `[{...}, {...}, {...}]`.
# url_custom = [{name = "IEEE Xplore", url = "http://ieeexplore.ieee.org/document/8039420/"}, {name = "Appendix", url = "http://ix.cs.uoregon.edu/~mingwei/zhang17iseismograph-appendix.pdf"}]

# Does the content use math formatting?
math = false

# Does the content use source code highlighting?
highlight = true

# Featured image
# Place your image in the `static/img/` folder and reference its filename below, e.g. `image = "example.jpg"`.
[header]
# image = "headers/bubbles-wide.jpg"
# caption = "My caption :smile:"

+++
