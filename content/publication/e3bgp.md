+++
title = "An Expectation-Based Approach to Policy-Based Security of the Border Gateway Protocol"
date = 2016-04-10
draft = false

# Authors. Comma separated list, e.g. `["Bob Smith", "David Jones"]`.
authors = ["Jun Li","Josh Stein","Mingwei Zhang","Olaf Maennel"]

# Publication type.
# Legend:
# 0 = Uncategorized
# 1 = Conference proceedings
# 2 = Journal
# 3 = Work in progress
# 4 = Technical report
# 5 = Book
# 6 = Book chapter
publication_types = ["1"]

# Publication name and optional abbreviated version.
publication = "In IEEE Conference on Computer Communications Workshops"
publication_short = "In IEEE *GLOBECOM*"

# Abstract and optional shortened version.
abstract = "The inter-domain routing protocol of the Internet, i.e., Border Gateway Protocol (BGP), is vulnerable to malicious attacks. Although many security solutions for BGP have been proposed, they have mainly focused on topology-based security. Policy-based security has largely been overlooked-a severe concern especially since BGP is a policy-based routing protocol. In this paper, we present an Expectation Exchange and Enforcement (E3) mechanism for defining policies between autonomous systems (ASes) such that any AS may enforce such policies."
abstract_short = ""

# Featured image thumbnail (optional)
image_preview = ""

# Is this a selected publication? (true/false)
selected = false

# Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter the filename (excluding '.md') of your project file in `content/project/`.
#   E.g. `projects = ["deep-learning"]` references `content/project/deep-learning.md`.
projects = []

# Tags (optional).
#   Set `tags = []` for no tags, or use the form `tags = ["A Tag", "Another Tag"]` for one or more tags.
tags = []

# Links (optional).
url_pdf = "http://ix.cs.uoregon.edu/~mingwei/li15expectation.pdf"
url_preprint = ""
url_code = ""
url_dataset = ""
url_project = ""
url_slides = ""
url_video = ""
url_poster = ""
url_source = ""

# Custom links (optional).
#   Uncomment line below to enable. For multiple links, use the form `[{...}, {...}, {...}]`.
# url_custom = [{name = "Custom Link", url = "http://example.org"}]
url_custom = [{name = "IEEE Xplore", url = "http://ieeexplore.ieee.org/document/7562098/"}]

# Does this page contain LaTeX math? (true/false)
math = false

# Does this page require source code highlighting? (true/false)
highlight = false

# Featured image
# Place your image in the `static/img/` folder and reference its filename below, e.g. `image = "example.jpg"`.
[header]
image = ""
caption = ""

+++
