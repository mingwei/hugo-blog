+++
title = "I-seismograph: Observing, Measuring, and Analyzing Internet Earthquakes"
date = "2017-09-18"

# Authors. Comma separated list, e.g. `["Bob Smith", "David Jones"]`.
authors = ["Mingwei Zhang", "Jun Li", "Scott Brooks"]

# Publication type.
# Legend:
# 0 = Uncategorized
# 1 = Conference proceedings
# 2 = Journal
# 3 = Work in progress
# 4 = Technical report
# 5 = Book
# 6 = Book chapter
publication_types = ["2"]

# Publication name and optional abbreviated version.
publication = "In *IEEE/ACM Transactions on Networking*."
publication_short = "In IEEE/ACM *ToN*"

# Abstract and optional shortened version.
abstract = "Disruptive events, such as large-scale power outages, undersea cable cuts, or security attacks, could have an impact on the Internet and cause the Internet to deviate from its normal state of operation, which we also refer to as an \"Internet earthquake.\" As the Internet is a large, complex moving target, unfortunately little research has been done to define, observe, quantify, and analyze such impact on the Internet, whether it is during a past event period or in real time. In this paper, we devise an Internet seismograph, or I-seismograph, to fill this gap. Since routing is the most basic function of the Internet and the Border Gateway Protocol (BGP) is the de facto standard inter-domain routing protocol, we focus on BGP to observe, measure, and analyze the Internet earthquakes. After defining what an impact to BGP entails, we describe how I-seismograph observes and measures the impact, exemplify its usage during both old and recent disruptive events, and further validate its accuracy and convergency. Finally, we show that I-seismograph can further be used to help analyze what happened to BGP while BGP experienced an impact, including which autonomous systems (AS) were affected most or which AS paths or path segments surged significantly in BGP updates during an Internet earthquake."
abstract_short = ""

# Featured image thumbnail (optional)
image_preview = ""

# Is this a selected publication? (true/false)
selected = true

# Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter the filename (excluding '.md') of your project file in `content/project/`.
# projects = ["iseismograph"]

# Links (optional).
url_pdf = "http://ix.cs.uoregon.edu/~mingwei/zhang17iseismograph.pdf"
# url_pdf = "http://eprints.soton.ac.uk/352095/1/Cushen-IMV2013.pdf"
# url_preprint = "http://eprints.soton.ac.uk/352095/1/Cushen-IMV2013.pdf"
# url_code = "#"
# url_dataset = "#"
# url_project = "#"
# url_slides = "#"
# url_video = "#"
# url_poster = "#"
# url_source = "#"

# Custom links (optional).
#   Uncomment line below to enable. For multiple links, use the form `[{...}, {...}, {...}]`.
url_custom = [{name = "IEEE Xplore", url = "http://ieeexplore.ieee.org/document/8039420/"}, {name = "Appendix", url = "http://ix.cs.uoregon.edu/~mingwei/zhang17iseismograph-appendix.pdf"}]

# Does the content use math formatting?
math = false

# Does the content use source code highlighting?
highlight = true

# Featured image
# Place your image in the `static/img/` folder and reference its filename below, e.g. `image = "example.jpg"`.
[header]
# image = "headers/bubbles-wide.jpg"
# caption = "My caption :smile:"

+++
